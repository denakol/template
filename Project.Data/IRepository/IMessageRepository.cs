﻿using Project.Data.Infrastructure;
using Project.Model.Models;

namespace Project.Data.IRepository
{
    public interface IMessageRepository : IRepository<Message>
    {

    }
}
